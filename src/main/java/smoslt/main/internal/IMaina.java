/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.main module, one of many modules that belongs to smoslt

 smoslt.main is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.main is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.main in the file smoslt.main/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.main.internal;

public interface IMaina{

	/**
	 * @param int
	 * @return
	 */
	public abstract String maina1a(int maina1aa);

	
}
