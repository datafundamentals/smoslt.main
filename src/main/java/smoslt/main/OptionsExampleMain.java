/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.main module, one of many modules that belongs to smoslt

 smoslt.main is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.main is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.main in the file smoslt.main/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.main;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Please do not remove this otherwise useless class.
 * 
 * This is an example program only for easy pickup of things like adding
 * options, comparing competing options, bringing in a properties file etc.
 * Otherwise it is a program that does absolutely nothing, and is only in this
 * project because this project is a throw-away project and not a part of the
 * production release system.
 * 
 * The code for this class was taken originally from Marcos's spike updater, all
 * credit to him for the research entailed.
 * 
 */
public class OptionsExampleMain {

    static Properties props;

    public static void main(String[] args) throws Exception {
        CommandLine cli = processArgsFromCommandLine(args);
        props = packageArgsIntoProperties(cli);
        System.out.println("loaderDsl: " + cli.getOptionValue("l"));
        System.out.println("Test Only?: "
                + (cli.hasOption("testonly") ? "Yes" : "No"));
        System.out.print("Input from: ");
        if (cli.hasOption("s")) {
            System.out.println("STDIN");
        } else {
            String inputFileName = cli.getOptionValue("f");
            System.out.println(inputFileName);
        }
        if (cli.hasOption("testonly")) {
            System.out.println("doSomting");
        }
        // The following does not apply unless you actually sucked in a
        // properties file with this stuff in it
        System.out.println("workFlowJsonFile: "
                + props.getProperty("workFlowJsonFile"));
        System.out.println("loaderDsl: " + props.getProperty("loaderDsl"));
        System.out.println("username:   " + props.getProperty("username"));
        System.out.println("password:   '" + props.getProperty("password"));
        System.out.println("Done with main");
        System.exit(0);
    }

    protected static Properties packageArgsIntoProperties(CommandLine cli)
            throws Exception {
        Properties externalProps = new Properties();
        if (cli.hasOption("p")) {
            try {
                String propFileName = cli.getOptionValue("p");
                externalProps.load(new FileInputStream(propFileName));
            } catch (IOException ex) {
                System.out.println("===> " + ex.getMessage());
                throw new Exception(ex.getMessage());
            }
        }
        return externalProps;
    }

    @SuppressWarnings("static-access")
    protected static CommandLine processArgsFromCommandLine(String[] args)
            throws ParseException {
        Options options = new Options();
        options.addOption(OptionBuilder
                .withArgName("fileLoaderMatcher")
                .hasArg()
                .withDescription(
                        "hash described in the form of a json file matching fileNamePrefix to workflow json file to be executed")
                .create("m"));
        options.addOption(new Option(
                "testonly",
                "test only -verifies that properties and input are read, but does not load to GAE"));
        options.addOption(OptionBuilder
                .withArgName("loaderDsl")
                .isRequired()
                .hasArg()
                .withDescription(
                        "loader Dsl Json File for generic loader options not specific to this workflow")
                .create("l"));
        options.addOption(new Option("h", "Usage information\n"));
        options.addOption(new Option("help", "Usage information\n"));
        options.addOption(OptionBuilder
                .withArgName("workFlowJsonFile")
                .hasArg()
                .withDescription(
                        "workflow json file that contains specific workflow to be executed")
                .create("w"));
        options.addOption(OptionBuilder
                .withArgName("inputFileName")
                .hasArg()
                .withDescription(
                        "file name of file to be run through workflow - over-rides any fileName found in the workflow json")
                .create("f"));
        CommandLineParser parser = new BasicParser();
        CommandLine line;
        try {
            line = parser.parse(options, args);
            Iterator<Option> it = line.iterator();

            while (it.hasNext()) {
                Option o = it.next();
                System.out.println("ARG: " + o.getOpt() + "=" + o.getValue());
            }
            System.out.println();
            if (line.hasOption("h") || line.hasOption("help")) {
                printUsage(options);
            } else if (!(line.hasOption("s") || line.hasOption("f"))) {
                throw new ParseException("Must use either -s or -f");
            }
        } catch (ParseException exp) {
            printUsage(options);
            throw new ParseException(exp.getMessage());
        }
        return line;
    }

    protected static void printUsage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(80);
        formatter.printHelp("FakeLoaderMainFromGAE", options);
    }

}
